import {Component, OnInit} from '@angular/core';
import {NexperiaDocument} from "./model/nexperiaDocument";
import {NexperiaDocumentService} from "./service/nexperia.document.service";
import {HttpErrorResponse} from "@angular/common/http";
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public nexperiaDocuments: NexperiaDocument[] | undefined;

  constructor(private nexperiaDocumentService: NexperiaDocumentService) {
  }

  ngOnInit(): void {
    // this.getDocuments();
  }

  public getDocuments(): void {
    this.nexperiaDocumentService.getNexperiaDocuments().subscribe(
      (response: NexperiaDocument[]) => {
        this.nexperiaDocuments = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    );
  }

  public onSearchDocuments(searchForm: NgForm): void {
    console.log(searchForm.value);
    console.log("hello!")
    this.nexperiaDocumentService.findNexperiaDocuments(searchForm.value.name).subscribe(
      (response: NexperiaDocument[]) => {
        this.nexperiaDocuments = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }
}
