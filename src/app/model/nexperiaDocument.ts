export interface NexperiaDocument {
  name: string;
  title: string;
  description: string;
  descriptiveTitle: string;
  documentType: string;
  publicationId: number;
  system: number;
  version: number;
  nodeUUID: string;
  downloadUrl: string;
  viewUrl: string;
}
