import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {NexperiaDocument} from "../model/nexperiaDocument";
import {environment} from "../../environments/environment";

@Injectable({providedIn: 'root'})
export class NexperiaDocumentService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  public getNexperiaDocuments(): Observable<NexperiaDocument[]> {
    return this.http.get<NexperiaDocument[]>(`${this.apiServerUrl}/document/all`);
  }

  public addNexperiaDocuments(NexperiaDocument: NexperiaDocument): Observable<NexperiaDocument> {
    return this.http.post<NexperiaDocument>(`${this.apiServerUrl}/document/add`, NexperiaDocument);
  }

  public updateNexperiaDocuments(NexperiaDocument: NexperiaDocument): Observable<NexperiaDocument> {
    return this.http.put<NexperiaDocument>(`${this.apiServerUrl}/document/update`, NexperiaDocument);
  }

  public deleteNexperiaDocuments(documentId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/document/delete/${documentId}`);
  }

  public findNexperiaDocuments(documentName: string): Observable<NexperiaDocument[]> {
    return this.http.get<NexperiaDocument[]>(`${this.apiServerUrl}/document/search/${documentName}`);
  }
}

